Rolespecific Node
-----------------
To install, place the entire rolespecific_node folder into your modules directory.
Go to Administer -> Site building -> Modules and enable the Rolespecific Node module

Now go to Administer -> Site configuration -> Rolespecific Node and configure to your
liking.

From now on a node of the specified type will be created or published if it exists 
already when a user gets assigned a certain role (by an administrator or by the auto 
 assign role module). This node will be unpublished when the user's role is taken away.